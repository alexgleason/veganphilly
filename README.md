# VeganPhilly

> :warning: **Warning:** This package is experimental and should not be relied on. There are no long term plans or strategies to maintain it.

VeganPhilly is a fork of [OpenVegeMap](https://github.com/Rudloff/openvegemap) 0.7.0 by Pierre Rudloff. It shows a map of restaurants with vegan options in the Philadelphia area, pulled from OpenStreetMap data.

Changes from OpenVegeMap:

* Only POIs with vegan options are shown. OpenVegeMap shows all POIs with `diet:*` set.
* 100% Vegan POIs are shown in green, 100% Vegetarian (with vegan options) are shown in orange, and vegan options shown in gray. By contrast, OpenVegeMap shows vegan options in green, vegetarian options in dark green, and nonveg options in red.
* The map is centered on Philly by default rather than Paris.
* The title and page metadata have been changed to reflect that.
* Map marker icons correspond with their color and have no additional meaning. In OpenVegeMap, the icons have an additional meaning.
* Min zoom level has been set to 13 from 15 so POIs will load when zoomed out more.

## OpenVegeMap

Please see [OpenVegeMap](https://github.com/Rudloff/openvegemap)'s README for details on building and more information about the project.
